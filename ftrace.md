# 1. ftrace - function trace

参考链接：https://docs.kernel.org/trace/ftrace.html#

## 1.1 ftrace

### 接口

| 接口 | 说明 |
| :-:  | :--  |
| tracing_on | ftrace开关，使用events或者各类tracer都要设置该值为1<br>设置或显示是否启用ftrace<br>写"0"到禁用ftrace，写"1"使能ftrace<br>禁用ftrace只是禁止写入环形缓冲区，跟踪开销仍在发生<br>可以在内核中使用内核函数 tracing_off() 来禁止写入环形缓冲区，这会将此文件设置为“0”<br>用户空间可以通过在文件中回显“1”来重新启用跟踪<br>function或event的triigger命令“traceoff”也会将此文件设置为零并停止跟踪 |
| trace | 该文件以可读的格式保存跟踪的输出<br>使用O_TRUNC标志打开此文件进行写入会清除环形缓冲区内容，此文件不是消费者<br/>如果ftrace被禁用（没有跟踪器运行包括各种tracer和event，或者 tracing_on 为零），每次读取时都会产生相同的输出<br/>当跟踪打开时，它可能会产生不一致的结果，因为它会尝试读取整个缓冲区 |
| trace_pipe | 输出与trace文件相同，但此文件旨在通过实时跟踪进行流式传输<br/>读取此文件将阻塞，直到检索到新数据<br/>与“trace”文件不同，这个文件是一个消费者，这意味着从此文件读取会导致顺序读取显示更多当前数据<br/>一旦从这个文件中读取数据，它就会被消耗掉，并且不会通过顺序读取再次读取<br/> “trace”文件是静态的，如果跟踪器没有添加更多数据，每次读取时都会显示相同的信息 |
| current_tracer | 常用tracer如下：<br/>function_graph: 在函数的入口和出口处进行跟踪，绘制类似于 C 代码源的函数调用图的功能。<br>function:抓取所有内核函数调用 <br>nop:不抓取任何函数调用 |
| set_event | 读该文件可以获取当前已使能的事件，也可以通过写该接口使能指定事件。 |
| set_event_pid | 过滤event pid |
| set_event_notrace_pid | |
| set_ftrace_filter | 设置过滤条件 |
| set_ftrace_notrace | |
| set_graph_function | 配合function_graph使用。设置跟踪指定函数或不跟踪指定函数 |
| set_graph_notrace | |
| set_ftrace_pid | 设置ftrace pid |
| trace_marker | 用于在trace或trace_pipe写入指定标志 |
| max_graph_depth | 如果使用function_graph，设置抓取函数的最大阶数。如果函数调用很深的话，合理设置这个参数可以让抓到的结果更易读 |

### 选项

| 选项 | 说明 |
| :-:  | :--  |
| event-fork | |
| function-fork | |
| func_stack_trace | |
| stacktrace | |
| sym-offset | |
| sym-addr | |
| sym-userobj | |
| funcgraph-cpu | 使用function_graph时，是否抓当前函数执行的CPU信息 |
| funcgraph-overhead | 使用function_graph时，显示是否该函数执行的耗时标志，比如：<br> '$' - greater than 1 second <br> '@' - greater than 100 millisecond <br> '*' - greater than 10 millisecond <br> '#' - greater than 1000 microsecond <br> '!' - greater than 100 microsecond <br> '+' - greater than 10 microsecond <br> ' ' - less than or equal to 10 microsecond <br> 可以在启动参数通过 nofunction-overhead 关闭该功能 |

## 1.2 输出格式

## 1.3 过滤命令

参考：https://docs.kernel.org/trace/ftrace.html#filter-commands

## 1.4 ftrace使用示例

### 1.4.1 使用function_graph

#### 1.4.1.1 基本用法：抓指定函数的调用过程

```
tracing_on           1
current_tracer       function_graph
---------------------------------------------
 
set_graph_function 
virtio_queue_rq [virtio_blk] 
```

显示如下：

```
  4)               |  virtio_queue_rq [virtio_blk]() {
  4)               |    virtblk_prep_rq.isra.0 [virtio_blk]() {
  4)               |      blk_mq_start_request() {
  4)   0.269 us    |        ktime_get();
  4)               |        __rq_qos_issue() {
  4)   0.139 us    |          wbt_issue();
  4)   0.467 us    |        }
  4)   0.129 us    |        blk_add_timer();
  4)   1.502 us    |      }
  4)   1.861 us    |    }
  4)               |    _raw_spin_lock_irqsave() {
  4)   0.159 us    |      preempt_count_add();
  4)   0.458 us    |    }
  4)               |    virtblk_add_req [virtio_blk]() {
  4)               |      virtqueue_add_sgs() {
  4)               |        __kmalloc() {
  4)   0.140 us    |          kmalloc_slab();
  4)               |          __kmem_cache_alloc_node() {
  4)   0.130 us    |            should_failslab();
  4)   0.448 us    |          }
  4)   0.955 us    |        }
  4)   0.120 us    |        vring_map_one_sg();
  4)   0.129 us    |        vring_map_one_sg();
  4)   0.119 us    |        vring_map_single.constprop.0();
  4)   2.139 us    |      }
  4)   2.508 us    |    }
  4)   0.149 us    |    virtqueue_kick_prepare();
  4)               |    _raw_spin_unlock_irqrestore() {
  4)   0.149 us    |      preempt_count_sub();
  4)   0.437 us    |    }
  4)               |    virtqueue_notify() {
  4)   3.612 us    |      vp_notify();
  4)   3.920 us    |    }
  4) + 10.648 us   |  }

```

#### 1.4.1.2 进阶1：抓指定函数的调用过程的同时，查看该函数的调用栈

参考：https://www.kernel.org/doc/html/latest/trace/ftrace.html#filter-commands

原理：function_graph与filter命令结合，filter命令func:stacetrace负责抓取栈，!func:stacktrace取消

```
tracing_on           1
current_tracer       function_graph
---------------------------------------------
 
set_graph_function 
virtio_queue_rq [virtio_blk] 

set_ftrace_filter 
virtio_queue_rq [virtio_blk]:stacktrace:unlimited
```

显示如下：

```
    kworker/2:1H-359     [002] ...2.  8167.117760: <stack trace>
 => ftrace_call
 => virtio_queue_rq
 => blk_mq_dispatch_rq_list
 => __blk_mq_sched_dispatch_requests
 => blk_mq_sched_dispatch_requests
 => __blk_mq_run_hw_queue
 => blk_mq_run_hw_queues
 => blk_mq_requeue_work
 => process_one_work
 => worker_thread
 => kthread
 => ret_from_fork
  2)               |  virtio_queue_rq [virtio_blk]() {
  2)               |    virtblk_prep_rq.isra.0 [virtio_blk]() {
  2)               |      blk_mq_start_request() {
  2)   0.298 us    |        ktime_get();
  2)               |        __rq_qos_issue() {
  2)   0.219 us    |          wbt_issue();
  2)   0.687 us    |        }
  2)   0.219 us    |        blk_add_timer();
  2)   2.070 us    |      }
  2)   2.597 us    |    }
  2)               |    _raw_spin_lock_irqsave() {
  2)   0.239 us    |      preempt_count_add();
  2)   0.627 us    |    }
  2)               |    virtblk_add_req [virtio_blk]() {
  2)               |      virtqueue_add_sgs() {
  2)               |        __kmalloc() {
  2)   0.209 us    |          kmalloc_slab();
  2)               |          __kmem_cache_alloc_node() {
  2)   0.209 us    |            should_failslab();
  2)   0.637 us    |          }
  2)   1.423 us    |        }
  2)   0.219 us    |        vring_map_one_sg();
  2)   0.209 us    |        vring_map_one_sg();
  2)   0.219 us    |        vring_map_single.constprop.0();
  2)   3.095 us    |      }
  2)   3.572 us    |    }
  2)   0.229 us    |    virtqueue_kick_prepare();
  2)               |    _raw_spin_unlock_irqrestore() {
  2)   0.219 us    |      preempt_count_sub();
  2)   0.617 us    |    }
  2)               |    virtqueue_notify() {
  2)   5.085 us    |      vp_notify();
  2)   5.583 us    |    }
  2) + 14.628 us   |  }
```

这种方式显示的栈在函数调用过程之前，并且不能显示函数参数。

#### 1.4.1.3 进阶2：抓指定函数的调用过程的同时，查看该函数的调用栈以及函数参数

使用kprobe-event抓函数参数，参考：https://docs.kernel.org/trace/kprobetrace.html

原理：kprboe-event抓参数和调用栈，function_graph抓函数调用过程

```
tracing_on           1
current_tracer       function_graph
---------------------------------------------
set_event 
kprobes:virtio_queue_rq 

kprobe_events 
p:kprobes/virtio_queue_rq virtio_queue_rq op=+24(+0($arg2)):s32 tag=+32(+0($arg2)):s32 internel_tag=+36(+0($arg2)):s32 nr_phys_segments=+124(+0($arg2)):s16 sector=+48(+0($arg2)):u64 len=+44(+0($arg2)):s32 

set_graph_function 
virtio_queue_rq [virtio_blk] 
stacktrace  1 
```

kprobe event命令如下：

```
echo 'p:virtio_queue_rq virtio_queue_rq op=+24(+0($arg2)):s32 tag=+32(+0($arg2)):s32 internel_tag=+36(+0($arg2)):s32 nr_phys_segments=+124(+0($arg2)):s16 sector=+48(+0($arg2)):u64 len=+44(+0($arg2)):s32' > kprobe_events
```

输出结果如下：

```
  1)               |  /* virtio_queue_rq: (virtio_queue_rq+0x0/0x1c0 [virtio_blk]) op=1048577 tag=24 internel_tag=-1 nr_phys_segments=1 sector=16493400 len=4096 */
    kworker/1:1H-461     [001] ...1. 11438.657697: <stack trace>
 => virtio_queue_rq
 => blk_mq_dispatch_rq_list
 => __blk_mq_sched_dispatch_requests
 => blk_mq_sched_dispatch_requests
 => __blk_mq_run_hw_queue
 => process_one_work
 => worker_thread
 => kthread
 => ret_from_fork
  1)               |  virtio_queue_rq [virtio_blk]() {
  1)               |    virtblk_prep_rq.isra.0 [virtio_blk]() {
  1)   0.467 us    |      __blk_rq_map_sg();
  1)               |      blk_mq_start_request() {
  1)   0.437 us    |        ktime_get();
  1)               |        __rq_qos_issue() {
  1)   0.378 us    |          wbt_issue();
  1)   2.567 us    |        }
  1)   0.418 us    |        blk_add_timer();
  1)   5.393 us    |      }
  1)   8.139 us    |    }
  1)               |    _raw_spin_lock_irqsave() {
  1)   0.398 us    |      preempt_count_add();
  1)   1.104 us    |    }
  1)               |    virtblk_add_req [virtio_blk]() {
  1)               |      virtqueue_add_sgs() {
  1)               |        __kmalloc() {
  1)   0.358 us    |          kmalloc_slab();
  1)               |          __kmem_cache_alloc_node() {
  1)   0.338 us    |            should_failslab();
  1)   1.055 us    |          }
  1)   2.418 us    |        }
  1)   0.359 us    |        vring_map_one_sg();
  1)   0.358 us    |        vring_map_one_sg();
  1)   0.349 us    |        vring_map_one_sg();
  1)   0.358 us    |        vring_map_single.constprop.0();
  1)   5.981 us    |      }
  1)   6.746 us    |    }
  1)   0.408 us    |    virtqueue_kick_prepare();
  1)               |    _raw_spin_unlock_irqrestore() {
  1)   0.368 us    |      preempt_count_sub();
  1)   1.045 us    |    }
  1)               |    virtqueue_notify() {
  1) + 14.439 us   |      vp_notify();
  1) + 15.393 us   |    }
  1) + 35.414 us   |  }
```

#### 1.4.1.4 进阶3：抓指定函数的调用过程，过滤掉不关注的函数，查看该函数的调用栈以及函数参数

上面抓到的调用过程有一些我们并不关注，比如spin_lock系列函数，可以通过set_graph_notrace过滤掉：echo '*spin_*' > set_graph_notrace

```
tracing_on           1
current_tracer       function_graph
---------------------------------------------

set_event 
kprobes:virtio_queue_rq 
 
kprobe_events 
p:kprobes/virtio_queue_rq virtio_queue_rq op=+24(+0($arg2)):s32 tag=+32(+0($arg2)):s32 internel_tag=+36(+0($arg2)):s32 nr_phys_segments=+124(+0($arg2)):s16 sector=+48(+0($arg2)):u64 len=+44(+0($arg2)):s32 

set_graph_function 
virtio_queue_rq [virtio_blk] 

set_graph_notrace 
_raw_spin_trylock_bh
hwspin_lock_get_id
__hwspin_unlock
_raw_spin_unlock
spin_lock_irqsave_ssp_contention
__hwspin_trylock
devm_hwspin_lock_device_match
__pv_queued_spin_unlock_slowpath
bpf_spin_lock
hwspin_lock_unregister_single
raw_spin_rq_unlock
rwsem_spin_on_owner
devm_hwspin_lock_register
spin_lock_irqsave_check_contention
_raw_spin_lock_bh
hwspin_lock_request
devm_hwspin_lock_unregister
hwspin_lock_register
hwspin_lock_request_specific
_raw_spin_unlock_irq
of_hwspin_lock_get_id
_raw_spin_trylock
devm_hwspin_lock_request
hwspin_lock_unregister
devm_hwspin_lock_match
__hwspin_lock_request
raw_spin_rq_lock_nested
bpf_spin_unlock
tasklet_unlock_spin_wait
devm_hwspin_lock_unreg
_raw_spin_unlock_irqrestore
mutex_spin_on_owner
_raw_spin_unlock_bh
of_hwspin_lock_get_id_byname
_raw_spin_lock_irq
devm_hwspin_lock_release
hwspin_lock_free
_raw_spin_lock
raw_spin_rq_trylock
devm_hwspin_lock_free
__pv_queued_spin_lock_slowpath
devm_hwspin_lock_request_specific
_raw_spin_lock_irqsave
__hwspin_lock_timeout
native_queued_spin_lock_slowpath
process_spin_lock 

stacktrace  1 
```

```
 => virtio_queue_rq
 => blk_mq_dispatch_rq_list
 => __blk_mq_sched_dispatch_requests
 => blk_mq_sched_dispatch_requests
 => __blk_mq_run_hw_queue
 => process_one_work
 => worker_thread
 => kthread
 => ret_from_fork
  3)               |  virtio_queue_rq [virtio_blk]() {
  3)               |    virtblk_prep_rq.isra.0 [virtio_blk]() {
  3)   0.239 us    |      __blk_rq_map_sg();
  3)               |      blk_mq_start_request() {
  3)   0.269 us    |        ktime_get();
  3)               |        __rq_qos_issue() {
  3)   0.219 us    |          wbt_issue();
  3)   0.647 us    |        }
  3)   0.229 us    |        blk_add_timer();
  3)   1.941 us    |      }
  3)   2.816 us    |    }
  3)               |    virtblk_add_req [virtio_blk]() {
  3)               |      virtqueue_add_sgs() {
  3)               |        __kmalloc() {
  3)   0.219 us    |          kmalloc_slab();
  3)               |          __kmem_cache_alloc_node() {
  3)   0.219 us    |            should_failslab();
  3)   0.627 us    |          }
  3)   1.632 us    |        }
  3)   0.219 us    |        vring_map_one_sg();
  3)   0.209 us    |        vring_map_one_sg();
  3)   0.218 us    |        vring_map_one_sg();
  3)   0.219 us    |        vring_map_single.constprop.0();
  3)   3.801 us    |      }
  3)   4.248 us    |    }
  3)   0.408 us    |    virtqueue_kick_prepare();
  3)               |    virtqueue_notify() {
  3) + 22.538 us   |      vp_notify();
  3) + 23.404 us   |    }
  3) + 32.519 us   |  }
```

#### 1.4.1.5 进阶4：抓指定函数的调用过程，只关注该函数调用的部分函数，查看该函数的调用栈以及函数参数

如果函数a调用栈很深，调用了很多函数，其中只有一部分是我们关注的函数，可以通过set_ftrace_filter过滤关注的函数。
通常可以用来抓指定函数的耗时，以及分析代码逻辑，过滤无用信息。

virtio_queue_rq的栈比较浅，我们可以看出它调用了下面函数

```
blk_add_timer
blk_mq_start_request
__blk_rq_map_sg
__kmalloc
kmalloc_slab
__kmem_cache_alloc_node
ktime_get
preempt_count_add
preempt_count_sub
_raw_spin_lock_irqsave
_raw_spin_unlock_irqrestore
__rq_qos_issue
should_failslab
virtblk_add_req
virtblk_prep_rq
virtqueue_add_sgs
virtqueue_kick_prepare
virtqueue_notify
vp_notify
vring_map_one_sg
vring_map_single.constprop.0
wbt_issue
```

但只想知道部分函数被调用的情况或耗时，比如

```
virtblk_add_req
virtqueue_add_sgs
virtqueue_kick_prepare
virtqueue_notify
```

那么我们可以这样设置：

```
tracing_on           1
current_tracer       function_graph
---------------------------------------------

set_event 
kprobes:virtio_queue_rq 

kprobe_events 
p:kprobes/virtio_queue_rq virtio_queue_rq op=+24(+0($arg2)):s32 tag=+32(+0($arg2)):s32 internel_tag=+36(+0($arg2)):s32 nr_phys_segments=+124(+0($arg2)):s16 sector=+48(+0($arg2)):u64 len=+44(+0($arg2)):s32 

set_graph_function 
virtio_queue_rq [virtio_blk] 

set_ftrace_filter 
virtqueue_notify
virtqueue_kick_prepare
virtqueue_add_sgs
virtblk_add_req [virtio_blk]
virtio_queue_rq [virtio_blk] 

stacktrace  1 
```

抓到的信息精简了很多：

```
  5)               |  /* virtio_queue_rq: (virtio_queue_rq+0x0/0x1c0 [virtio_blk]) op=2048 tag=148 internel_tag=-1 nr_phys_segments=1 sector=141503024 len=4096 */
   rs:main Q:Reg-1058    [005] ...1. 20355.474231: <stack trace>
 => virtio_queue_rq
 => __blk_mq_try_issue_directly
 => blk_mq_try_issue_directly
 => blk_mq_submit_bio
 => __submit_bio
 => submit_bio_noacct_nocheck
 => submit_bio_wait
 => iomap_read_folio_sync
 => iomap_write_begin
 => iomap_file_buffered_write
 => xfs_file_buffered_write
 => vfs_write
 => ksys_write
 => do_syscall_64
 => entry_SYSCALL_64_after_hwframe
  5)               |  virtio_queue_rq [virtio_blk]() {
  5)               |    virtblk_add_req [virtio_blk]() {
  5)   1.522 us    |      virtqueue_add_sgs();
  5)   2.428 us    |    }
  5)   0.329 us    |    virtqueue_kick_prepare();
  5) + 11.373 us   |    virtqueue_notify();
  5) + 16.548 us   |  }

```

# 2. event trace

参考：https://docs.kernel.org/trace/events.html

## 2.1 使能event trace格式

## 2.2 event filter

## 2.3 event trigger

# 3. 基于kprobe的event

参考：https://docs.kernel.org/trace/kprobetrace.html

### 3.1 格式

```
 p[:[GRP/][EVENT]] [MOD:]SYM[+offs]|MEMADDR [FETCHARGS]        : Set a probe
 r[MAXACTIVE][:[GRP/][EVENT]] [MOD:]SYM[+0] [FETCHARGS]        : Set a return probe
 p[:[GRP/][EVENT]] [MOD:]SYM[+0]%return [FETCHARGS]    : Set a return probe
 -:[GRP/][EVENT]                                               : Clear a probe

GRP            : Group name. If omitted, use "kprobes" for it.
EVENT          : Event name. If omitted, the event name is generated
                 based on SYM+offs or MEMADDR.
MOD            : Module name which has given SYM.
SYM[+offs]     : Symbol+offset where the probe is inserted.
SYM%return     : Return address of the symbol
MEMADDR        : Address where the probe is inserted.
MAXACTIVE      : Maximum number of instances of the specified function that
                 can be probed simultaneously, or 0 for the default value
                 as defined in Documentation/trace/kprobes.rst section 1.3.1.

FETCHARGS      : Arguments. Each probe can have up to 128 args.
 %REG          : Fetch register REG
 @ADDR         : Fetch memory at ADDR (ADDR should be in kernel)
 @SYM[+|-offs] : Fetch memory at SYM +|- offs (SYM should be a data symbol)
 $stackN       : Fetch Nth entry of stack (N >= 0)
 $stack        : Fetch stack address.
 $argN         : Fetch the Nth function argument. (N >= 1) (\*1)
 $retval       : Fetch return value.(\*2)
 $comm         : Fetch current task comm.
 +|-[u]OFFS(FETCHARG) : Fetch memory at FETCHARG +|- OFFS address.(\*3)(\*4)
 \IMM          : Store an immediate value to the argument.
 NAME=FETCHARG : Set NAME as the argument name of FETCHARG.
 FETCHARG:TYPE : Set TYPE as the type of FETCHARG. Currently, basic types
                 (u8/u16/u32/u64/s8/s16/s32/s64), hexadecimal types
                 (x8/x16/x32/x64), "char", "string", "ustring", "symbol", "symstr"
                 and bitfield are supported.

 (\*1) only for the probe on function entry (offs == 0).
 (\*2) only for return probe.
 (\*3) this is useful for fetching a field of data structures.
 (\*4) "u" means user-space dereference. See :ref:`user_mem_access`.
```

### 3.2 从参数获取变量值

### 3.3 probe任意位置并获取变量值

获取指定结构的相对偏移量，例如需要获得```scsi_cmnd``` 结构的```retries```成员相当偏移量。

方式1：gdb print命令

```
print (int)&((struct xxx *)0)->xxx
```
例如：
```
print (int)&((struct scsi_cmnd *)0)->retries
```

![image-20230417214901743](images/ftrace/image-20230417214901743.png)

方式2：gdb ptype命令

```
ptype /o struct xxx
```

说明：/o选项在新版本的gdb才支持

例如：

![image-20230417214312326](images/ftrace/image-20230417214312326.png)


方式3：crash struct命令

```
struct xxx -o
```
例如：

![image-20230417214415736](images/ftrace/image-20230417214415736.png)

### 3.4 解引用指针内容

# 4. boot time tracing

参考链接：https://docs.kernel.org/admin-guide/kernel-parameters.html#kernelparameters

## 4.1 ftrace

### 启动参数说明

| 参数 | 格式 | 说明 |
| :-- | :-- | :-- |
| ftrace | ftrace=tracer | 指定tracer，可以设置下面值：<br> function_graph<br> nop<br> function |
| trace_options | trace_options=[option-list] | 参考前面的章节，比如<br>trace_options=stacktrace |
| ftrace_filter | ftrace_filter=[function-list]<br> function list是以逗号分割的函数列表 | 类似 set_ftrace_filter 接口，如果设置了则只有该接口设置的函数才会被跟踪 |
| ftrace_notrace | ftrace_notrace=[function-list]<br> function list是以逗号分割的函数列表 | 类似 set_ftrace_notrace，在该接口的函数不会被跟踪 |
| ftrace_graph_filter | ftrace_graph_filter=[function-list]<br> function list是以逗号分割的函数列表 | 类似 set_graph_function，在该接口的函数才会被跟踪function_graph跟踪 |
| ftrace_graph_notrace | ftrace_graph_notrace=[function-list]<br> function list是以逗号分割的函数列表 | 类似 set_graph_notrace，在该接口的函数不会被跟踪function_graph跟踪 |
| ftrace_graph_max_depth | ftrace_graph_max_depth=[uint] | 设置function_graph跟踪时的最大深度，默认为0 |
| tp_printk | tp_printk | 使能tracepoint_printk，对应接口为: /proc/sys/kernel/tracepoint_printk；该选项使能后，tracepoint的打印会直接打印到系统日志，通过关闭tracing_on也无法关闭打印到系统日志的tracepoint信息 |


### 参数生效时机

上述参数主要在```kernel/trace/trace.c```和 ```kernel/trace/ftrace.c```完成初始化，初始化如下：

kernel/trace/trace.c

解析命令行参数:ftrace=xxx和trace_options
```
static int __init set_cmdline_ftrace(char *str)
{
        strscpy(bootup_tracer_buf, str, MAX_TRACER_SIZE);
        default_bootup_tracer = bootup_tracer_buf;
        /* We are using ftrace early, expand it */
        ring_buffer_expanded = true;
        return 1;
}
__setup("ftrace=", set_cmdline_ftrace)

// 解析命令行参数:trace_options=xxx
static int __init set_trace_boot_options(char *str)
{
	strlcpy(trace_boot_options_buf, str, MAX_TRACER_SIZE);
	return 1;
}
__setup("trace_options=", set_trace_boot_options);
```

kernel/trace/ftrace.c
解析ftrace_filter/ftrace_notrace/ftrace_graph_filter/ftrace_graph_notrace/ftrace_graph_max_depth等命令行参数

```
static int __init set_ftrace_notrace(char *str)
{
	ftrace_filter_param = true;
	strlcpy(ftrace_notrace_buf, str, FTRACE_FILTER_SIZE);
	return 1;
}
__setup("ftrace_notrace=", set_ftrace_notrace);

static int __init set_ftrace_filter(char *str)
{
	ftrace_filter_param = true;
	strlcpy(ftrace_filter_buf, str, FTRACE_FILTER_SIZE);
	return 1;
}
__setup("ftrace_filter=", set_ftrace_filter);

static int __init set_graph_function(char *str)
{
	strlcpy(ftrace_graph_buf, str, FTRACE_FILTER_SIZE);
	return 1;
}
__setup("ftrace_graph_filter=", set_graph_function);

static int __init set_graph_notrace_function(char *str)
{
	strlcpy(ftrace_graph_notrace_buf, str, FTRACE_FILTER_SIZE);
	return 1;
}
__setup("ftrace_graph_notrace=", set_graph_notrace_function);

static int __init set_graph_max_depth_function(char *str)
{
	if (!str)
		return 0;
	fgraph_max_depth = simple_strtoul(str, NULL, 0);
	return 1;
}
__setup("ftrace_graph_max_depth=", set_graph_max_depth_function);
```

init/main.c
```
// 注册指定trace并使能对应的trace_options
start_kernel()
    -> ftrace_init()
        -> set_ftrace_early_filters()       // ftrace相关的filter和notrace在这时生效
                                            // 但是ftrace=xxx还未生效
            -> ftrace_set_early_filter()
            -> set_ftrace_early_graph()
    -> early_trace_init()
        -> tracer_alloc_buffers()
            -> register_tracer(&nop_trace) // 注册ftrace=xxx指定的trace
				-> apply_trace_boot_options();
			-> apply_trace_boot_options(); // 使能trace_options=xxx指定的options
```

### 参数执行效果

![image-20230416135541429](images/ftrace/image-20230416135541429.png)

注意：使用本参数需要配合```ftrace_graph_filter```和```ftrace_graph_notrace```使用，否则会有很多日志。

## 4.2 trace event

### 启动参数说明

| 参数 | 格式 | 说明 |
| :-- | :-- | :-- |
| trace_event | trace_event=[event-list] | 各个event之间以","分隔 |
| trace_trigger | trace_trigger=[trigger-list] | 各个event trigger之间以","分隔 |

说明：如果使用trace_trigger的stacktrace功能，需要加上下面选项：trace_options=stacktrace

### 参数生效时机

```
start_kernel()
	-> trace_init()
		-> trace_event_init()
			-> event_trace_memsetup()
			-> init_ftrace_syscalls()
			-> event_trace_enable()
			-> event_trace_init_fields()
	-> arch_call_rest_init()
		-> rest_init() 		// 通过user_mode_thread调用 kernel_init
			-> kernel_init()
				-> kernel_init_freeable()
					-> do_basic_setup()
						-> do_initcalls()
							-> do_one_initcall()
								-> event_trace_enable_again // 再次使能trace event
															// early_initcall
```

## 4.3 kprobe based events

### 启动参数说明

| 参数 | 格式 | 说明 |
| :-- | :-- | :-- |
| trace_trigger | kprobe_event=[probe-list] | 与非boot kprobe event格式类似，但是使用","替换非boot kprobe event的空格<br>各个event trigger之间以";"分隔 |

比如：
```
kprobe_event=p:scsi_host_alloc,scsi_host_alloc;p:blk_mq_alloc_tag_set,blk_mq_alloc_tag_set
```

### kprobe based event初始化时机

```
start_kernel()
	-> arch_call_rest_init()
		-> rest_init() 		// 通过user_mode_thread调用 kernel_init
			-> kernel_init()
				-> kernel_init_freeable()
					-> do_basic_setup()
						-> do_initcalls()
							-> do_one_initcall()
								-> init_kprobe_trace()
									-> setup_boot_kprobe_events()
										-> enable_boot_kprobe_events()
```

### 内核启动后效果

![image-20230414101211617](images/ftrace/image-20230414101211617.png)

## 4.4 全局trace相关初始化视图

```
start_kernel()
    -> ftrace_init()
        -> set_ftrace_early_filters()       			// ftrace相关的filter和notrace在这时生效
														// 但是ftrace=xxx还未生效
            -> ftrace_set_early_filter()
            -> set_ftrace_early_graph()
    -> early_trace_init()
        -> tracer_alloc_buffers()
            -> register_tracer(&nop_trace)				// 注册ftrace=xxx开始生效
				-> apply_trace_boot_options();
			-> apply_trace_boot_options()				// trace_options=xxx开始生效
	-> trace_init()
		-> trace_event_init()
			-> event_trace_memsetup()
			-> init_ftrace_syscalls()
			-> event_trace_enable()						// trace_event=xxx开始生效
				-> __trace_early_add_events()
					-> __trace_early_add_new_event()
						-> trace_early_triggers()
							-> trigger_process_regex()	// trace_trigger=xxx开始生效
														// kprobe event没有这个流程
														// 所以trace_trigger对kprobe_event不生效
			-> event_trace_init_fields()
	-> arch_call_rest_init()
		-> rest_init() 		// 通过user_mode_thread调用 kernel_init
			-> kernel_init()
				-> kernel_init_freeable()
					-> do_basic_setup()
						-> do_initcalls()
							-> do_one_initcall()
								-> init_kprobe_trace()					// initcall级别为fs_initcall
									-> setup_boot_kprobe_events()
										-> enable_boot_kprobe_events()	// kprobe event开始生效
```

## 4.5 注意事项

### 4.5.1 boot时trace_clock不是global，系统启动后ring_buffer数据被情况

原因分析：

```
__init static void tracing_set_default_clock(void)
{
	/* sched_clock_stable() is determined in late_initcall */
	if (!trace_boot_clock && !sched_clock_stable()) {	// 判断如果boot_clock与sched_clock_stable不一致就会切
														// clock，切clock时会刷掉ring_buffer的内容
		if (security_locked_down(LOCKDOWN_TRACEFS)) {
			pr_warn("Can not set tracing clock due to lockdown\n");
			return;
		}

		printk(KERN_WARNING
		       "Unstable clock detected, switching default tracing clock to \"global\"\n"
		       "If you want to keep using the local clock, then add:\n"
		       "  \"trace_clock=local\"\n"
		       "on the kernel command line\n");
		tracing_set_clock(&global_trace, "global");
	}
}

__init static int late_trace_init(void)
{
	if (tracepoint_printk && tracepoint_printk_stop_on_boot) {
		static_key_disable(&tracepoint_printk_key.key);
		tracepoint_printk = 0;
	}

	tracing_set_default_clock();
	clear_boot_tracer();
	return 0;
}
late_initcall_sync(late_trace_init);
```

解决方案：

指定trace_clock：

```
trace_clock=global
```

### 4.5.2 trace_trigger对kprobe based events无法生效

原因分析：
见4.4流程分析，因为使能trace_trigger的流程在使能kprobe based event之前，因此不会生效。

# 5. 汇编相关

## 5.1 x86相关寄存器

| Register | Purpose                                | Saved across calls | name in kprobe |
| -------- | -------------------------------------- | ------------------ | :------------- |
| %rax     | temp register; return value            | No                 | %ax            |
| %rbx     | callee-saved                           | Yes                |                |
| %rcx     | used to pass 4th argument to functions | No                 | %cx            |
| %rdx     | used to pass 3rd argument to functions | No                 | %dx            |
| %rsp     | stack pointer                          | Yes                |                |
| %rbp     | callee-saved; base pointer             | Yes                |                |
| %rsi     | used to pass 2nd argument to functions | No                 | %si            |
| %rdi     | used to pass 1st argument to functions | No                 | %di            |
| %r8      | used to pass 5th argument to functions | No                 |                |
| %r9      | used to pass 6th argument to functions | No                 |                |
| %r10-r11 | temporary                              | No                 |                |
| %r12-r15 | callee-saved registers                 | Yes                |                |

probe 指定地址：

如果某地址有代码如下：

```
crash> dis -l do_sys_open
/source/mainline/fs/open.c: 1370
0xffffffffa86878a7 <do_sys_open>:       call   0xffffffffc0745000
0xffffffffa86878ac <do_sys_open+5>:     push   %r12
0xffffffffa86878ae <do_sys_open+7>:     mov    %rsi,%r12
0xffffffffa86878b1 <do_sys_open+10>:    mov    %edx,%esi
/source/mainline/fs/open.c: 1371
0xffffffffa86878b3 <do_sys_open+12>:    movzwl %cx,%edx
/source/mainline/fs/open.c: 1370
0xffffffffa86878b6 <do_sys_open+15>:    push   %rbp
0xffffffffa86878b7 <do_sys_open+16>:    mov    %edi,%ebp
0xffffffffa86878b9 <do_sys_open+18>:    sub    $0x20,%rsp
0xffffffffa86878bd <do_sys_open+22>:    mov    %gs:0x28,%rax
0xffffffffa86878c6 <do_sys_open+31>:    mov    %rax,0x18(%rsp)
0xffffffffa86878cb <do_sys_open+36>:    xor    %eax,%eax
/source/mainline/fs/open.c: 1371
0xffffffffa86878cd <do_sys_open+38>:    mov    %rsp,%rdi
0xffffffffa86878d0 <do_sys_open+41>:    call   0xffffffffa8687364 <build_open_how>
/source/mainline/fs/open.c: 1372
0xffffffffa86878d5 <do_sys_open+46>:    mov    %r12,%rsi
0xffffffffa86878d8 <do_sys_open+49>:    mov    %ebp,%edi
0xffffffffa86878da <do_sys_open+51>:    mov    %rsp,%rdx
0xffffffffa86878dd <do_sys_open+54>:    call   0xffffffffa86875bd <do_sys_openat2>
/source/mainline/fs/open.c: 1373
0xffffffffa86878e2 <do_sys_open+59>:    mov    0x18(%rsp),%rdx
0xffffffffa86878e7 <do_sys_open+64>:    sub    %gs:0x28,%rdx
0xffffffffa86878f0 <do_sys_open+73>:    je     0xffffffffa86878f7 <do_sys_open+80>
0xffffffffa86878f2 <do_sys_open+75>:    call   0xffffffffa8e885f3 <__stack_chk_fail>
0xffffffffa86878f7 <do_sys_open+80>:    add    $0x20,%rsp
0xffffffffa86878fb <do_sys_open+84>:    pop    %rbp
0xffffffffa86878fc <do_sys_open+85>:    pop    %r12
0xffffffffa86878fe <do_sys_open+87>:    ret
0xffffffffa86878ff <do_sys_open+88>:    int3
0xffffffffa8687900 <do_sys_open+89>:    int3
0xffffffffa8687901 <do_sys_open+90>:    int3
0xffffffffa8687902 <do_sys_open+91>:    int3
```

我们如果probe地址```do_sys_open+16``` 即是在该指令之前打印。

## 5.2 x86 栈帧组合的栈布局

* 当寄存器充足时，可能函数的局部变量不会在栈上分配，而是直接给定某个寄存器代表该变量
* 在把callee-saved寄存器入栈后，可能会在栈预留一些空间供后续使用
* 函数入口处入栈，入栈的内容包括rbp，callee saved寄存器（x86机器使用call转移时，函数的返回地址也会被压入栈上，这个动作不是由编译器显式完成，而是由call指令同步完成）
* 函数出口处按照相反的方向出栈，对于由call指令压入栈中的返回地址则由ret隐式出栈
* 当寄存器充足时，函数内定义的局部变量不一定在栈上分配，可能直接给定某个寄存器代表该变量；如果该变量在栈上分配，则在后面不会对该局部变量做入栈、出栈等操作，而是直接以%rbp或%rsp的偏移量寻址该局部变量
* 在调用子函数时，如果参数数量大于能够使用的传参寄存器数（ARM 8个，X86 6个）时，会把参数压入栈中，子函数从栈中获取参数，在子函数调用完成后，调用者自行负责把压栈的参数pop出来（比如scsi_vpd_inquiry调用scsi_execute_cmd）
* 函数调用过程栈的处理有2种方式：方式1：栈帧组合（-fno-omit-frame-pointer）； 方式2：只使用sp，不用fp（-fomit-frame-pointer）
* 使用栈帧组合的方式，可以轻松通过sp/fp分析函数调用过程；只是用栈的情况需要结合debug信息或反汇编信息从栈中分析函数调用过程
* x86 kernel可以通过UNWINDER_FRAME_POINTER配置项使能栈帧组合的函数调用。
```https://gcc.gnu.org/onlinedocs/gcc/Using-Assembly-Language-with-C.html```

* gcc可能做如下优化：如果函数A为static函数，且该函数在本文件内一直被以相同的参数被调用，则会把函数foo优化为foo.constprop.x，该函数的栈不再按照标准函数分布，caller会直接调用foo，而不传递参数(参数在foo内部写死)。在写测试代码时，为了防止被优化，可以直接把函数通过EXPORT_SYMBOL导出。比如:

```
static noinline void func2(int a, int b, int c, int d, int f, int g, int h, int i)
{
        printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
                __func__, a, b, c, d, f, g, h, i);
}
static noinline void func1(int a, int b, int c, int d, int f, int g, int h, int i)
{
        func2(0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27);
}

对应汇编代码如下：

crash> dis func2.constprop.0
0xffffffffc04450a6 <func2.constprop.0>: nopl   0x0(%rax,%rax,1) [FTRACE NOP]
0xffffffffc04450ab <func2.constprop.0+5>:       push   %rbp
0xffffffffc04450ac <func2.constprop.0+6>:       mov    $0x23,%r9d
0xffffffffc04450b2 <func2.constprop.0+12>:      mov    $0x22,%r8d
0xffffffffc04450b8 <func2.constprop.0+18>:      mov    $0x21,%ecx
0xffffffffc04450bd <func2.constprop.0+23>:      mov    $0x20,%edx
0xffffffffc04450c2 <func2.constprop.0+28>:      mov    $0xffffffffc0449125,%rsi
0xffffffffc04450c9 <func2.constprop.0+35>:      mov    $0xffffffffc0449010,%rdi
0xffffffffc04450d0 <func2.constprop.0+42>:      mov    %rsp,%rbp
0xffffffffc04450d3 <func2.constprop.0+45>:      push   $0x27
0xffffffffc04450d5 <func2.constprop.0+47>:      push   $0x26
0xffffffffc04450d7 <func2.constprop.0+49>:      push   $0x25
0xffffffffc04450d9 <func2.constprop.0+51>:      push   $0x24
0xffffffffc04450db <func2.constprop.0+53>:      call   0xffffffff872df716 <_printk>
0xffffffffc04450e0 <func2.constprop.0+58>:      add    $0x20,%rsp
0xffffffffc04450e4 <func2.constprop.0+62>:      leave
0xffffffffc04450e5 <func2.constprop.0+63>:      ret
0xffffffffc04450e6 <func2.constprop.0+64>:      int3
0xffffffffc04450e7 <func2.constprop.0+65>:      int3
0xffffffffc04450e8 <func2.constprop.0+66>:      int3
0xffffffffc04450e9 <func2.constprop.0+67>:      int3
crash> dis func1.constprop.0
0xffffffffc044531a <func1.constprop.0>: nopl   0x0(%rax,%rax,1) [FTRACE NOP]
0xffffffffc044531f <func1.constprop.0+5>:       push   %rbp
0xffffffffc0445320 <func1.constprop.0+6>:       mov    %rsp,%rbp
0xffffffffc0445323 <func1.constprop.0+9>:       call   0xffffffffc04450a6 <func2.constprop.0> //直接调用func2，调用之前没设置参数
0xffffffffc044532d <func1.constprop.0+19>:      pop    %rbp
0xffffffffc044532e <func1.constprop.0+20>:      ret
0xffffffffc044532f <func1.constprop.0+21>:      int3
0xffffffffc0445330 <func1.constprop.0+22>:      int3
0xffffffffc0445331 <func1.constprop.0+23>:      int3
0xffffffffc0445332 <func1.constprop.0+24>:      int3
```

### 5.2.1 布局示意图

![mm-x86_frame_sp](images/ftrace/mm-x86_frame_sp.png)

函数A调用了函数B，函数B的rbp为rbp_b。函数B返回到函数A的地址为：```*(rbp_b + 8)```，函数A的rbp为：```*rbp```，函数A的栈的范围是：```[rbp_b + 16, *rbp + 8]```

### 5.2.2 实际分析：以iscsid进程为例分析栈帧演变

下面实例中，被```__schedule```调用的函数的rbp不可知，但是```__schedule```的rpb是已知的，即```ffffbc9647e4b9b0```

调用```__schedule```的函数是```schedule```，因此```schedule```的rbp是```*ffffbc9647e4b9b0 = ffffbc9647e4b9d0```，schedule的栈的范围是:```[ffffbc9647e4b9c0, ffffbc9647e4b9d8]```
调用```schedule```的函数是```schedule_hrtimeout_range_clock```，因此```schedule_hrtimeout_range_clock```的rbp是```*ffffbc9647e4b9d0 = ffffbc9647e4ba48```，栈的范围是:```[ffffbc9647e4b9e0, ffffbc9647e4ba50]```
调用```schedule_hrtimeout_range_clock```的函数是```schedule_hrtimeout_range```，因此```schedule_hrtimeout_range```的rbp是```*ffffbc9647e4ba48 = ffffbc9647e4ba58```，栈的范围是:```[ffffbc9647e4ba58, ffffbc9647e4ba60]```
调用```schedule_hrtimeout_range```的函数是```poll_schedule_timeout```，因此```poll_schedule_timeout```的rbp是```*ffffbc9647e4ba58 = ffffbc9647e4ba70```，栈的范围是:```[ffffbc9647e4ba68, ffffbc9647e4ba78]```

![image-20230705234642507](images/ftrace/image-20230705234642507.png)

![image-20230706000946717](images/ftrace/image-20230706000946717.png)

### 5.2.3 编写程序dumpstack并观察stack

代码如下：
```
#include <stdio.h>

#define context_info() \
do { \
	register unsigned long rbp asm ("r12"); \
	register unsigned long rsp asm ("r13"); \
	asm ("mov %rbp, %r12"); \
	asm ("mov %rsp, %r13"); \
	printf("[%s], rbp: 0x%lx, rsp: 0x%lx\n", __func__, rbp, rsp); \
} while (0)

void dump_stack(unsigned long rbp)
{
	int i = 0;
	unsigned long stack_s;
	unsigned long stack_e;
	unsigned long *p;

	stack_s = rbp + 16;
	stack_e = *(unsigned long *)rbp + 8;

	for (p = (unsigned long *)stack_s; p <= (unsigned long *)stack_e; p++, i++) {
		if (i % 2 == 0)
			printf("0x%016x: ", p);
		printf("0x%016lx ", *p);

		if (i % 2)
			printf("\n");
	}
}

void func3(int a, int b, int c, int d, int f, int g, int h, int hi)
{
	int i = 0;
	unsigned long *p;
	register unsigned long rbp asm ("r12");
	register unsigned long rsp asm ("r13");
	asm ("mov %rbp, %r12");
	asm ("mov %rsp, %r13");

	printf("[%s], rbp: 0x%lx, rsp: 0x%lx\n", __func__, rbp, rsp);
	printf("\nfunc3's stack:\n");
	for (p = (unsigned long *)rsp; p <= (unsigned long *)(rbp + 8); p++, i++) {
		if (i % 2 == 0)
			printf("0x%016x: ", p);
		printf("0x%016lx ", *p);
		if (i % 2)
			printf("\n");
	}

	printf("\nfunc2's stack:\n");
	dump_stack(rbp);

	rbp = *(unsigned long *)rbp;
	printf("\nfunc1's stack:\n");
	dump_stack(rbp);

	rbp = *(unsigned long *)rbp;
	printf("\nmain's stack:\n");
	dump_stack(rbp);
}

void func2(int a, int b, int c, int d, int f, int g, int h, int i)
{
	context_info();

	func3(0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37);
}

void func1(int a, int b, int c, int d, int f, int g, int h, int i)
{
	context_info();

	func2(0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27);
}

int main(void)
{
	register unsigned long rbp asm ("r12"); \
	register unsigned long rsp asm ("r13"); \
	asm ("mov %rbp, %r12"); \
	asm ("mov %rsp, %r13"); \

	printf("main is 0x%lx\n", main);
	printf("func1 is 0x%lx\n", func1);
	printf("func2 is 0x%lx\n", func2);
	printf("func3 is 0x%lx\n", func3);

	printf("[%s], rbp: 0x%lx, rsp: 0x%lx\n", __func__, rbp, rsp); \

	func1(0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17);

	return 0;
}
```

运行结果：

![image-20230706154030735](images/ftrace/image-20230706154030735.png)

### 5.2.4 通过ko观察：

ko代码如下：

定义了2个sysfs接口，分别为：```/sys/kernel/crash_stack/stack``` 和 ```/sys/kernel/crash_stack/control```

control是一个整型数据，默认为0，可以直接通过control接口读写该整型数据；
对stack的读写会判断control是否为0，如果是0，则读写stack的进程就会被阻塞，方便观察其栈。echo 非零数据到 control即可完成所有阻塞在stack的进程。

```
#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/wait.h>

static struct kobject *example_kobj;
static int control;
static int stack;
static DECLARE_WAIT_QUEUE_HEAD(wq);

void dstack(unsigned long rbp)
{
	int i = 0;
	unsigned long stack_s;
	unsigned long stack_e;
	unsigned long *p;

	stack_s = rbp + 16;
	stack_e = *(unsigned long *)rbp + 8;

	for (p = (unsigned long *)stack_s; p <= (unsigned long *)stack_e; p++, i++) {
		if (i % 2 == 0)
			printk(KERN_CONT "0x%lx: ", (unsigned long)p);
		printk(KERN_CONT "0x%016lx ", *p);

		if (i % 2)
			printk(KERN_CONT "\n");
	}
}
EXPORT_SYMBOL(dstack);

void func3(int a, int b, int c, int d, int f, int g, int h, int hi)
{
	int i = 0;
	unsigned long *p;
	register unsigned long rbp asm ("r12");
	register unsigned long rsp asm ("r13");
	asm ("mov %rbp, %r12");
	asm ("mov %rsp, %r13");

	printk("[%s], rbp: 0x%lx, rsp: 0x%lx\n", __func__, rbp, rsp);
	printk("\nfunc3's stack:\n");
	for (p = (unsigned long *)rsp; p <= (unsigned long *)(rbp + 8); p++, i++) {
		if (i % 2 == 0)
			printk(KERN_CONT "0x%lx: ", (unsigned long)p);
		printk(KERN_CONT "0x%016lx ", *p);
		if (i % 2)
			printk(KERN_CONT "\n");
	}

	printk("\nfunc2's stack:\n");
	dstack(rbp);

	rbp = *(unsigned long *)rbp;
	printk(KERN_CONT "\nfunc1's stack:\n");
	dstack(rbp);

	rbp = *(unsigned long *)rbp;
	printk("\nmain's stack:\n");
	dstack(rbp);

	wait_event_interruptible(wq, control);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, hi);
}
EXPORT_SYMBOL(func3);

void func2(int a, int b, int c, int d, int f, int g, int h, int i)
{
	func3(0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, i);
}
EXPORT_SYMBOL(func2);

void func1(int a, int b, int c, int d, int f, int g, int h, int i)
{
	func2(0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, i);
}
EXPORT_SYMBOL(func1);

static ssize_t control_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
	sscanf(buf, "%d\n", &control);
	if (control)
		wake_up_interruptible(&wq);
        return count;
}
static ssize_t control_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	return sysfs_emit(buf, "%d\n", control);
}
static struct kobj_attribute control_attribute = __ATTR_RW(control);

static ssize_t stack_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
	func1(0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17);
	sscanf(buf, "%d\n", &stack);
        return count;
}
static ssize_t stack_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	func1(0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17);
	return sysfs_emit(buf, "%d\n", stack);
}
static struct kobj_attribute stack_attribute = __ATTR_RW(stack);

static struct attribute *attrs[] = {
	&control_attribute.attr,
	&stack_attribute.attr,
	NULL,
};
static struct attribute_group attr_group = {
	.attrs = attrs,
};

static int __init example_init(void)
{
	int retval;

	example_kobj = kobject_create_and_add("crash_stack", kernel_kobj);
	if (!example_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(example_kobj, &attr_group);
	if (retval)
		kobject_put(example_kobj);

	return retval;
}

static void __exit example_exit(void)
{
	kobject_put(example_kobj);
}

module_init(example_init);
module_exit(example_exit);
MODULE_LICENSE("GPL");
```

运行结果：

![image-20230729102059827](images/ftrace/image-20230729102059827.png)
![image-20230729102601218](images/ftrace/image-20230729102601218.png)

## 5.3 arm64相关寄存器

## 5.4 arm64 栈帧组合的栈布局

### 5.4.1 布局示意图

![stack_layout](images/ftrace/stack_layout.svg)

### 5.4.2 实际分析：以vim进程为例分析栈帧演变

![arm64_crash_stack_analysis](images/ftrace/arm64_crash_stack_analysis.png)

### 5.4.3  编写程序dumpstack并观察stack

```
#include <stdio.h>

#define context_info() \
do { \
	register unsigned long rbp asm ("x19"); \
	register unsigned long rsp asm ("x20"); \
	asm ("mov x19, fp"); \
	asm ("mov x20, sp"); \
	printf("[%s], rbp: 0x%lx, rsp: 0x%lx, return addr: 0x%lx\n", __func__, rbp, rsp, *(unsigned long *)(rbp + 8)); \
} while (0)

void dump_stack(unsigned long rbp)
{
	int i = 0;
	unsigned long stack_s;
	unsigned long stack_e;
	unsigned long *p;

	stack_s = rbp;
	stack_e = *(unsigned long *)rbp;

	for (p = (unsigned long *)stack_s; p < (unsigned long *)stack_e; p++, i++) {
		if (i % 2 == 0)
			printf("0x%016x: ", p);
		printf("0x%016lx ", *p);

		if (i % 2)
			printf("\n");
	}
}

void dump_hex(unsigned long rbp)
{
	int i = 0;
	unsigned long *p = (unsigned long *)rbp;

	printf("0x%016x, 0x%016x\n", p, rbp);

	for (i = 0; i < 20; p++, i++) {
		if (i % 2 == 0)
			printf("0x%016x: ", p);
		printf("0x%016lx ", *p);

		if (i % 2)
			printf("\n");
	}
}

void func3(int a, int b, int c, int d, int e, int f, int g, int h, int hi, int j)
{
	int i = 0;
	unsigned long *p;
	register unsigned long rbp asm ("x19");
	register unsigned long rsp asm ("x20");
	asm ("mov x19, fp");
	asm ("mov x20, sp");

	printf("[%s], rbp: 0x%lx, rsp: 0x%lx, return addr: 0x%lx\n", __func__, rbp, rsp, *(unsigned long *)(rbp + 8)); 

	printf("\nfunc3's stack:\n");
	for (p = (unsigned long *)rsp; p < (unsigned long *)(*(unsigned long *)rbp); p++, i++) {
		if (i % 2 == 0)
			printf("0x%016x: ", p);
		printf("0x%016lx ", *p);
		if (i % 2)
			printf("\n");
	}

	rbp = *(unsigned long *)rbp;
	printf("\nfunc2's stack:\n");
	dump_stack(rbp);

	rbp = *(unsigned long *)rbp;
	printf("\nfunc1's stack:\n");
	dump_stack(rbp);

	rbp = *(unsigned long *)rbp;
	printf("\nmain's stack:\n");
	dump_stack(rbp);
}

void func2(int a, int b, int c, int d, int e, int f, int g, int h, int hi, int j)
{
	context_info();

	func3(0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39);
}

void func1(int a, int b, int c, int d, int e, int f, int g, int h, int hi, int j)
{
	context_info();

	func2(0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29);
}

int main(void)
{
	context_info();

	func1(0x20, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19);

	return 0;
}
```



运行结果如下：

![image-20230729154327860](images/ftrace/image-20230729154327860.png)

### 5.4.4 通过ko观察

```
#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/wait.h>

static struct kobject *example_kobj;
static int control;
static int stack;
static DECLARE_WAIT_QUEUE_HEAD(wq);

void dstack(unsigned long rbp)
{
	int i = 0;
	unsigned long stack_s;
	unsigned long stack_e;
	unsigned long *p;

	stack_s = rbp;
	stack_e = *(unsigned long *)rbp;

	for (p = (unsigned long *)stack_s; p < (unsigned long *)stack_e; p++, i++) {
		if (i % 2 == 0)
			printk(KERN_CONT "0x%lx: ", (unsigned long)p);
		printk(KERN_CONT "0x%016lx ", *p);

		if (i % 2)
			printk(KERN_CONT "\n");
	}
}
EXPORT_SYMBOL(dstack);

void func3(int a, int b, int c, int d, int f, int g, int h, int hi)
{
	int i = 0;
	unsigned long *p;
	register unsigned long rbp asm ("x19");
	register unsigned long rsp asm ("x20");
	asm ("mov x19, fp");
	asm ("mov x20, sp");

	printk("[%s], rbp: 0x%lx, rsp: 0x%lx, return addr: 0x%lx\n", __func__, rbp, rsp, *(unsigned long *)(rbp + 8));
	printk("\nfunc3's stack:\n");
	for (p = (unsigned long *)rbp; p < (unsigned long *)(*(unsigned long *)rbp); p++, i++) {
		if (i % 2 == 0)
			printk(KERN_CONT "0x%lx: ", (unsigned long)p);
		printk(KERN_CONT "0x%016lx ", *p);
		if (i % 2)
			printk(KERN_CONT "\n");
	}

	rbp = *(unsigned long *)rbp;
	printk("\nfunc2's stack:\n");
	dstack(rbp);

	rbp = *(unsigned long *)rbp;
	printk(KERN_CONT "\nfunc1's stack:\n");
	dstack(rbp);

	rbp = *(unsigned long *)rbp;
	printk("\nmain's stack:\n");
	dstack(rbp);

	wait_event_interruptible(wq, control);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, hi);
}
EXPORT_SYMBOL(func3);

void func2(int a, int b, int c, int d, int f, int g, int h, int i)
{
	func3(0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, i);
}
EXPORT_SYMBOL(func2);

void func1(int a, int b, int c, int d, int f, int g, int h, int i)
{
	func2(0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27);
	printk("%s: 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x\n",
		__func__, a, b, c, d, f, g, h, i);
}
EXPORT_SYMBOL(func1);

static ssize_t control_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
	sscanf(buf, "%d\n", &control);
	if (control)
		wake_up_interruptible(&wq);
        return count;
}
static ssize_t control_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	return sysfs_emit(buf, "%d\n", control);
}
static struct kobj_attribute control_attribute = __ATTR_RW(control);

static ssize_t stack_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
	func1(0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17);
	sscanf(buf, "%d\n", &stack);
        return count;
}
static ssize_t stack_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	func1(0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17);
	return sysfs_emit(buf, "%d\n", stack);
}
static struct kobj_attribute stack_attribute = __ATTR_RW(stack);

static struct attribute *attrs[] = {
	&control_attribute.attr,
	&stack_attribute.attr,
	NULL,
};
static struct attribute_group attr_group = {
	.attrs = attrs,
};

static int __init example_init(void)
{
	int retval;

	example_kobj = kobject_create_and_add("crash_stack", kernel_kobj);
	if (!example_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(example_kobj, &attr_group);
	if (retval)
		kobject_put(example_kobj);

	return retval;
}

static void __exit example_exit(void)
{
	kobject_put(example_kobj);
}

module_init(example_init);
module_exit(example_exit);
MODULE_LICENSE("GPL");
```

# 6. crash trace command

参考链接：https://crash-utility.github.io/extensions.html
https://access.redhat.com/solutions/239433

使用方法：

1. 安装crash-trace-command

一般发行版都提供了该包，比如fedora以及openeuler，如果没提供可以到github下载源码自行编译：https://github.com/fujitsu/crash-trace

2. 在 crash 中加载trace.so

如果是rpm包提供的，通过 ```rpm -ql crash-trace-command``` 查询trace.so路径；如果是手动构建，trace.so路径在构建源码路径。之后在crash输入extend + trace.so

3. 在 crash 中打印ftrace信息

```
trace show
```

# 参考链接

http://6.s081.scripts.mit.edu/sp18/x86-64-architecture-guide.html
https://docs.oracle.com/cd/E19455-01/806-3773/instructionset-67/index.html
https://docs.oracle.com/cd/E19455-01/806-3773/instructionset-66/index.html
https://zhuanlan.zhihu.com/p/27339191
https://gcc.gnu.org/onlinedocs/gcc-4.9.2/gcc/Optimize-Options.html
https://yosefk.com/blog/getting-the-call-stack-without-a-frame-pointer.html
https://developer.arm.com/documentation/dui0068/b/Assembler-Reference/Predefined-register-and-coprocessor-names/Predeclared-register-names
https://developer.arm.com/documentation/den0024/a/The-ABI-for-ARM-64-bit-Architecture
https://medium.com/geekculture/linux-cpu-context-switch-deep-dive-764bfdae4f01
https://azeria-labs.com/functions-and-the-stack-part-7/

